package com.example.setting.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.setting.R
import com.example.setting.model.SettingItem

class SettingAdapter constructor(
    private val listSetting: List<SettingItem>,
    private val onPositionClick: (position: Int) -> Unit
) :
    RecyclerView.Adapter<SettingAdapter.SettingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_item_content_setting, parent, false)
        return SettingViewHolder(view, onPositionClick)
    }

    override fun getItemCount(): Int {
        return listSetting.size
    }

    override fun onBindViewHolder(holder: SettingViewHolder, position: Int) {
        listSetting[position].let { holder.bindingData(it) }
    }

    class SettingViewHolder(
        itemView: View,
        private val onClickPosition: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {
        private val tvContent: TextView by lazy {
            itemView.findViewById(R.id.tv_title) as TextView
        }

        init {
            itemView.setOnClickListener {
                onClickPosition.invoke(adapterPosition)
            }
        }

        fun bindingData(settingItem: SettingItem) {
            tvContent.text = settingItem.name
        }
    }
}