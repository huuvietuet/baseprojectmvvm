package com.example.setting.ui.setting

import com.example.core.base.BaseViewModel
import com.example.core.utils.SingleLiveEvent
import com.example.core.utils.TypeNotifyDataSetChange
import com.example.setting.model.SettingItem
import javax.inject.Inject

class SettingViewModel @Inject constructor() : BaseViewModel() {

    var typeNotifyAdapter = SingleLiveEvent<TypeNotifyDataSetChange>()

    val listSetting = ArrayList<SettingItem>()

    init {
        listSetting.add(SettingItem("ahihi1"))
        listSetting.add(SettingItem("ahihi2"))
        listSetting.add(SettingItem("ahihi3"))
        listSetting.add(SettingItem("ahihi4"))
        listSetting.add(SettingItem("ahihi5"))
        listSetting.add(SettingItem("ahihi6"))
        listSetting.add(SettingItem("ahihi7"))
        listSetting.add(SettingItem("ahihi8"))
        listSetting.add(SettingItem("ahihi9"))
        listSetting.add(SettingItem("ahihi10"))

        typeNotifyAdapter.value = TypeNotifyDataSetChange.NotifyDataSetChange

    }

    fun onClickItem(position: Int) {
        listSetting.removeAt(position)
        typeNotifyAdapter.value = TypeNotifyDataSetChange.NotifyDataRemove(position)

    }
}