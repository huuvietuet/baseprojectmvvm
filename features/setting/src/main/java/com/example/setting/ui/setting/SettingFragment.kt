package com.example.setting.ui.setting

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.core.base.BaseFragment
import com.example.core.utils.TypeNotifyDataSetChange
import com.example.setting.R
import com.example.setting.SettingNavigation
import com.example.setting.adapter.SettingAdapter
import com.example.setting.databinding.FragmentSettingBinding
import javax.inject.Inject

class SettingFragment :
    BaseFragment<SettingViewModel, FragmentSettingBinding>() {

    @Inject
    lateinit var settingNavigation: SettingNavigation

    private var settingAdapter: SettingAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (binding.listSetting.adapter == null) {
            settingAdapter = SettingAdapter(viewModel.listSetting) { position ->
                viewModel.onClickItem(position)
            }
            binding.listSetting.adapter = settingAdapter
        }

        binding.background.setOnClickListener {
            settingNavigation.openNotice()
        }

        viewModel.typeNotifyAdapter.observe(viewLifecycleOwner, Observer {
            when (it) {
                is TypeNotifyDataSetChange.NotifyDataSetChange -> {
                    settingAdapter?.notifyDataSetChanged()
                }
                is TypeNotifyDataSetChange.NotifyDataInsert -> {
                    settingAdapter?.notifyItemInserted(it.position)
                }
                is TypeNotifyDataSetChange.NotifyDataInsertRange -> {
                    settingAdapter?.notifyItemRangeInserted(it.position, it.count)
                }
                is TypeNotifyDataSetChange.NotifyDataRemove -> {
                    settingAdapter?.notifyItemRemoved(it.position)
                }
                is TypeNotifyDataSetChange.NotifyDataRemoveRange -> {
                    settingAdapter?.notifyItemRangeRemoved(it.position, it.count)
                }
            }
        })
    }


    override fun layoutId(): Int {
        return R.layout.fragment_setting
    }

    override fun viewModelClass(): Class<SettingViewModel> {
        return SettingViewModel::class.java
    }
}