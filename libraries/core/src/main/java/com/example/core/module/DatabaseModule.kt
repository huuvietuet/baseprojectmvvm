package com.example.core.module

import com.example.core.database.DatabaseRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(databaseRepository: DatabaseRepository): DatabaseRepository {
        return databaseRepository
    }
}