package com.example.project.ui.mainScreen.profile

import com.example.core.base.BaseFragment
import com.example.recyclerviewmvvm.R
import com.example.recyclerviewmvvm.databinding.FragmentProfileBinding

class ProfileFragment : BaseFragment<ProfileViewModel, FragmentProfileBinding>() {
    override fun layoutId() = R.layout.fragment_profile

    override fun viewModelClass() = ProfileViewModel::class.java
}