package com.example.project.ui.mainScreen.home

import android.os.Bundle
import android.view.View
import com.example.core.base.BaseFragment
import com.example.project.navigation.AppNavigation
import com.example.recyclerviewmvvm.R
import com.example.recyclerviewmvvm.databinding.FragmentHomeBinding
import javax.inject.Inject

class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>() {

    @Inject
    lateinit var appNavigation: AppNavigation

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.background.setOnClickListener {
            appNavigation.openSettingScreen()
        }

    }

    override fun layoutId() = R.layout.fragment_home

    override fun viewModelClass() = HomeViewModel::class.java
}