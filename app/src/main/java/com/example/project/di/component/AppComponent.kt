package com.example.project.di.component

import com.example.core.module.ApiModule
import com.example.core.module.DatabaseModule
import com.example.core.module.RxModule
import com.example.core.module.SharedPrefsModule
import com.example.project.Application
import com.example.project.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ServiceModule::class,
        ClassModule::class,
        ViewModelModule::class,
        RxModule::class,
        SharedPrefsModule::class,
        DatabaseModule::class,
        NavigationModule::class,
        ApiModule::class
    ]
)
interface AppComponent : AndroidInjector<Application> {

    override fun inject(application: Application)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}