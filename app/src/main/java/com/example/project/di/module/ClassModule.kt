package com.example.project.di.module

import com.example.project.ui.main.MainActivity
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ClassModule {
    @Provides
    @Singleton
    @Named("MainActivity")
    fun provideMainClass(): Class<*> {
        return MainActivity::class.java
    }
}
